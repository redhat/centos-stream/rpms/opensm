#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

osmtest -p
ret=$?

exit $ret
